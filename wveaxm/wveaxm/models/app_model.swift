
class AppModel extends Model {
  // Variables
  late AppInfo appInfo;

  /// Create Singleton factory for [AppModel]
  ///
  static final AppModel _appModel = AppModel._internal();
  factory AppModel() {
    return _appModel;
  }
  AppModel._internal();
  // End

  /// Set data to AppInfo object
  void setAppInfo(Map<String, dynamic> json) {
    appInfo = AppInfo.fromJson(json);
    notifyListeners();
    debugPrint('AppInfo object -> updated!');
  }
}
