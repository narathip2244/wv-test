
class AppInfo {
  /// Variables
  final int androidAppCurrentVersion;
  final int windowAppCurrentVersion;
  final int iosAppCurrentVersion;
  final int macosAppCurrentVersion;
  final String androidPackageName;
  final String iOsAppId;
  final String appEmail;
  final String privacyPolicyUrl;
  final String termsOfServicesUrl;
  final String passwordResetLink;
  final String registerLink;

  /// Constructor
  AppInfo({
    required this.androidAppCurrentVersion,
    required this.windowAppCurrentVersion,
    required this.iosAppCurrentVersion,
    required this.macosAppCurrentVersion,
    required this.androidPackageName,
    required this.iOsAppId,
    required this.appEmail,
    required this.privacyPolicyUrl,
    required this.termsOfServicesUrl,
    required this.passwordResetLink,
    required this.registerLink,
  });

  /// factory AppInfo object
  factory AppInfo.fromJson(Map<String, dynamic> json) {
    // log(json.toString());
    return AppInfo(
      androidAppCurrentVersion: json[ANDROID_APP_CURRENT_VERSION] ?? 1,
      windowAppCurrentVersion: json[WINDOW_APP_CURRENT_VERSION] ?? 1,
      iosAppCurrentVersion: json[IOS_APP_CURRENT_VERSION] ?? 1,
      macosAppCurrentVersion: json[MACOS_APP_CURRENT_VERSION] ?? 1,
      androidPackageName: json[ANDROID_PACKAGE_NAME] ?? '',
      iOsAppId: (json[IOS_APP_ID] ?? 0).toString(),
      appEmail: json[APP_EMAIL] ?? '',
      privacyPolicyUrl: json[PRIVACY_POLICY_URL] ?? '',
      termsOfServicesUrl: json[TERMS_OF_SERVICE_URL] ?? '',
      passwordResetLink: json[APP_PASSWORD_RESET_LINK] ?? '',
      registerLink: json[APP_REGISTER_LINK] ?? '',
    );
  }
}
