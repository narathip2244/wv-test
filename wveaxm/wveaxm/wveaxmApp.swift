//
//  wveaxmApp.swift
//  wveaxm
//
//  Created by na on 12/6/2567 BE.
//

import SwiftUI

@main
struct wveaxmApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
